import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasePictureDetailComponent } from './src/basecomponent.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';

export * from './src/basecomponent.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
    BasePictureDetailComponent
  ],
  exports: [
    BasePictureDetailComponent
  ]
})
export class PictureDetailModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PictureDetailModule,
      providers: []
    };
  }
}
